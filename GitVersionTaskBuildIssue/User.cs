﻿using System;

namespace GitVersionTaskBuildIssue
{
    public class User
    {
        //User ID
        public int Id { get; set; }

        //User name
        public string Name { get; set; }

        //Active flag
        public bool IsActive { get; set; }
    }
}
